%--------------------------------------
%Authors: Dane Linsky, Brian Yurk
%--------------------------------------


%Initial point for distributions to be made. Between 0 and 1 Exclusive.
initialPoint = .6;

timeScale = 2;
CNtimeSteps = 100000;
numOfIndividuals = 1000;
CNpatchPts = 10000;

rWalkTime = CNtimeSteps/timeScale;
rPatchPts = 1000;

lambdaSpace = linspace(0,1,rPatchPts);
lambda = lambdaSpace(2)-lambdaSpace(1);
tau = linspace(0,1,rWalkTime);
tau = tau(2)-tau(1);

cnSpace = linspace(0,1,CNpatchPts);
dxc=cnSpace(2)-cnSpace(1);

vect = CrankNicolson(CNtimeSteps,numOfIndividuals, CNpatchPts,lambda, tau, initialPoint);
rVect = RandomWalk2(rWalkTime,rPatchPts,numOfIndividuals,initialPoint);

for plotInd = 1:size(rVect,2)
    clf;
    hold on
    plot(cnSpace,dxc*cumtrapz(vect(:,plotInd*timeScale)),'r');
    plot(lambdaSpace,cumtrapz(rVect(:,plotInd)),'b');
    ylim([0 1000]);
    legend({'Diffusion PDE', 'Random Walk'},'Location','northwest');
    hold off
    drawnow
%   M(plotInd) = getframe(gcf);
end

% writerObj = VideoWriter('pd.avi');
% writerObj.FrameRate = 10;
% open(writerObj);
% for mInd = 1:length(M);
%     writeVideo(writerObj,M(mInd));
% end
% close(writerObj);

