function [ transportVector] = RandomWalk2(timeSteps, pw, numIndv, initialPoint)
R = .33;
L = .66;
N = 1;

plotThresh = 100;

%Create the step size for a given individual
x = linspace(0,1,pw);
delX = x(2)-x(1);

%Create the time step given between moves
t = linspace(0,1,timeSteps);
delT = t(2)-t(1);

startInd = abs(x-initialPoint);
startInd = find(startInd==min(startInd));

%Setup matrix for storing results. Each row is a time step
%Each column is a position on the grid
%Number stored is the number of individuals at that point
transportVector = zeros(pw, round(timeSteps/plotThresh));
transportVector(round(pw/2),1) = numIndv;
indvVect(1:numIndv,1) = startInd;
for tInd = 1:timeSteps
   movement = rand(numIndv,1);
   movement((movement <= R)) = 1;
   movement((movement <= L) & (movement > R)) = -1;
   movement((movement < N) & (movement>L)) = 0;
   zInd = (indvVect <= 1);
   mInd = (indvVect >= pw);
   indvVect = indvVect + movement;
   indvVect(zInd) = 1;
   indvVect(mInd) = pw;
   if(mod(tInd,plotThresh)==0)
        for moveInd = 1:numIndv
            transportVector(indvVect(moveInd),tInd/plotThresh) = ...
                transportVector(indvVect(moveInd),tInd/plotThresh) + 1;
       end
       %transportVector(:,tInd/plotThresh) = (transportVector(:,tInd/plotThresh)/delX);
       transportVector(:,tInd/plotThresh) = (transportVector(:,tInd/plotThresh));
   end
end

RandomWalk = transportVector;
end

