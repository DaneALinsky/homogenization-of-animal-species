function [ solnVect ] = CrankNicolson(timeStepTotal,numOfIndividuals,patchPts,lambda, tau, initialPoint, timeThreshHold)

if (nargin == 6)
    timeThreshHold = timeStepTotal;
end
    
deviation = .01;
variance = deviation^2;
x = linspace(0,1,patchPts);
deltaX = x(2)-x(1);
%d = (numOfIndividuals/sqrt(2*pi*.1^2))*(exp((-(.02)^2)/(2*variance))*((-.02)/(2*variance))^2 + ...
    %exp((-(1)^2)/(2*variance))*(-2/(2*variance)));
diffCo = ((lambda^2)/tau)*(1-.34)/2;
t = linspace(0,1,timeStepTotal);
deltaT = t(2)-t(1);
mew = diffCo*(deltaT/(deltaX*deltaX));
midDiag(1:patchPts,1) = (1+mew);
outterDiag(1:patchPts,1) = (-mew)/2;
lhm = horzcat(outterDiag,midDiag,outterDiag);
lhm = spdiags(lhm,[-1 0 1],patchPts,patchPts);
lhm(1,1) = (1+mew);
lhm(1,2) = (-mew)/2;

midDiag(1:patchPts,1) = (1-mew);
outterDiag(1:patchPts,1) = (mew)/2;
rhm = horzcat(outterDiag,midDiag,outterDiag);
rhm = spdiags(rhm,[-1 0 1],patchPts,patchPts);
rhm(1,1) = (1-mew);
rhm(1,2) = (mew)/2;
p0 = (numOfIndividuals/(sqrt(2*pi*(variance)))) * exp((-((x-initialPoint).^2))/(2*(variance)));
p(1:patchPts,1) = p0;

solnVect(:,1) = p0;

for stepNum = 1:timeThreshHold;
    pp = mldivide(lhm,(rhm*p));
    p = pp;

     if(mod(stepNum,100)==0)
          solnVect = horzcat(solnVect,p);
     end
end

CrankNicolson = solnVect;
end

